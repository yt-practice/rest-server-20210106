import type { Paths, IService, Req, Rep } from '../gen'

export class Service implements IService {
	async getUsers(req: Req, rep: Rep): Promise<Paths.GetUsers.Responses.$200> {
		return []
	}
	async postUser(req: Req, rep: Rep): Promise<Paths.PostUser.Responses.$200> {
		return {
			id: 1,
			email: 'hoge',
			name: 'hoge',
		}
	}
}
