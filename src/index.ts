import Fastify from 'fastify'
import openapiGlue from 'fastify-openapi-glue'
import openapi from './gen/openapi'
import { Service } from './service'

const fastify = Fastify({ logger: true })
fastify.register(openapiGlue, {
	specification: openapi,
	service: new Service(),
	// securityHandlers: path.join(__dirname, '/security'),
	// prefix: 'v1',
	noAdditional: true,
	ajvOptions: {
		// formats: {
		// 	'custom-format': /\d{2}-\d{4}/,
		// },
	},
})

export const main = async () => {
	fastify.listen(3000, (x, address) => {
		x ? console.error(x) : console.log(`read on ${address}`)
	})
}

main().catch(x => {
	console.log('# something happens.')
	console.error(x)
	if ('undefined' === typeof process) return
	process.exit(1)
})
