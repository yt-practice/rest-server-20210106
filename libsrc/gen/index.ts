import { promises as fs } from 'fs'
import * as path from 'path'
import yaml from 'yaml'
import * as dtsgenerator from 'dtsgenerator'
import { createInterface } from './createInterface'

const main = async () => {
	const __root = path.join(__dirname, '../..')
	await fs
		.rmdir(path.join(__root, 'src/gen'), { recursive: true })
		.catch(() => void 0)
	await fs.mkdir(path.join(__root, 'src/gen'))
	const p = yaml.parse(
		await fs.readFile(path.join(__root, 'openapi.yml'), 'utf-8'),
	)
	await fs.writeFile(
		path.join(__root, 'src/gen/openapi.ts'),
		[
			'export const openapi = ' + JSON.stringify(p) + ' as const',
			'export default openapi',
		].join('\n') + '\n',
	)
	const d = await dtsgenerator.default({
		contents: [dtsgenerator.parseSchema(p)],
	})
	const content = [
		d.replace(/(^|\n)(\s*)(declare |)namespace /giu, '$1$2export namespace '),
	].join('\n')
	await fs.writeFile(path.join(__root, 'src/gen/output.ts'), content)
	const itertext = await createInterface(content)
	await fs.writeFile(path.join(__root, 'src/gen/IService.ts'), itertext)
	await fs.writeFile(
		path.join(__root, 'src/gen/index.ts'),
		[
			"export type { FastifyRequest as Req, FastifyReply as Rep } from 'fastify'",
			"export * from './output'",
			"export { openapi } from './openapi'",
			"export type { IService } from './IService'",
		].join('\n') + '\n',
	)
}

main().catch(x => {
	console.error(x)
	process.exit(1)
})
