import * as ts from 'typescript'

export const createInterface = async (code: string) => {
	const source = createSource(code, 'hoge')
	const list: Operation[] = []
	for (const node of getChildren(source)) {
		if (isModule(node) && 'Paths' === node.name.escapedText) {
			for (const item of walk(node)) {
				list.push(item)
			}
		}
	}
	return (
		[
			"import type { FastifyRequest as Req, FastifyReply as Rep } from 'fastify'",
			"import type { Paths } from './output'",
			'export interface IService {',
			...list.map(
				op =>
					`  ${op.path
						.map(s => s.replace(/^./, m => m.toLowerCase()))
						.join('.')}(req: Req, rep: Rep): Promise<${op.responses
						.map(r => ['Paths', ...op.path, 'Responses', r].join('.'))
						.join(' | ')}>`,
			),
			'}',
		].join('\n') + '\n'
	)
}

type Module = ts.ModuleDeclaration & {
	name: ts.Identifier
	body: ts.ModuleBlock
}

const isModule = (node: ts.Node): node is Module =>
	ts.isModuleDeclaration(node) &&
	ts.isIdentifier(node.name) &&
	!!node.body &&
	ts.isModuleBlock(node.body)

const getChildren = (node: ts.Node) => {
	const list: ts.Node[] = []
	ts.forEachChild(node, n => {
		list.push(n)
	})
	return list
}

type Operation = {
	path: string[]
	responses: string[]
}

const walk = function* (
	node: Module,
	path: string[] = [],
): Iterable<Operation> {
	for (const item of getChildren(node.body)) {
		if (!isModule(item)) continue
		let responses: false | string[] = false
		for (const node of getChildren(item.body)) {
			if (isModule(node) && 'Responses' === node.name.escapedText) {
				responses = getChildren(node.body).map(
					n => '' + ((n as ts.TypeAliasDeclaration)?.name?.escapedText || ''),
				)
				break
			}
		}
		if (responses) {
			yield { path: [...path, `${item.name.escapedText}`], responses }
		} else {
			for (const n of walk(item, [...path, `${item.name.escapedText}`])) {
				yield n
			}
		}
	}
}

const createSource = (code: string, name: string) =>
	ts.createSourceFile(
		name,
		code,
		ts.ScriptTarget.ES2019,
		false,
		ts.ScriptKind.TS,
	)
